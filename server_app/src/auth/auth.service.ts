import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserLoginDto } from 'src/user/dto/user-login.dto';
import { UserService } from 'src/user/user.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';

@Injectable()
export class AuthService {

    constructor(
        private userService: UserService,
        private jwtService: JwtService
    ) { }

    async validateUserByPassword(loginAttempt: UserLoginDto) {
        // let userToValidate = await this.userService.findOneByRut(loginAttempt.rut);
        let userToValidate = await this.userService.findUserPassword(loginAttempt.rut, loginAttempt.password);
        if (userToValidate) {
            return userToValidate;
        } else {
            throw new UnauthorizedException();
        }
        // return new Promise((resolve) => {
        //     userToValidate.checkPassword(loginAttempt.password, (err, isMatch) => {
        //         if (err) throw new UnauthorizedException();
        //         if (isMatch) {
        //             resolve(this.createJwtPayload(userToValidate));
        //         } else {
        //             throw new UnauthorizedException();
        //         }
        //     });
        // });
    }

    async validateUserByJwt(payload: JwtPayload) {
        let user = await this.userService.findOneByRut(payload.rut);
        if (user) {
            return this.createJwtPayload(user);
        } else {
            throw new UnauthorizedException();
        }
    }

    createJwtPayload(user) {
        let data: JwtPayload = { rut: user.rut };
        let jwt = this.jwtService.sign(data);
        return {
            expiresIn: 3600,
            token: jwt
        }
    }
}