import { Body, Controller, Post } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { UserLoginDto } from 'src/user/dto/user-login.dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService) {}

    @ApiUseTags('Login')
    @Post('login') 
    async login(@Body() loginUserDto: UserLoginDto){
        return await this.authService.validateUserByPassword(loginUserDto);
    }
}
