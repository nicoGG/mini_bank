import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDTO } from './dto/user.dto';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { User } from './interface/user.interface';

@Injectable()
export class UserService {

    constructor(
        @InjectModel('User') private userModel: Model<User>
    ) { }

    async findAll(): Promise<User[]> {
        return await this.userModel.find().exec();
    }

    async find(id: string) {
        return await this.userModel.find({ _id: id }).exec();
    }

    async findOneByRut(rut: string): Promise<User> {
        return await this.userModel.findOne({ rut: rut });
    }

    async findOneByRutName(rut: string, first_name: string, last_name: string): Promise<User> {
        return await this.userModel.findOne({ rut: rut, first_name: first_name, last_name: last_name });
    }

    async findUserPassword(rut: string, password: string): Promise<User> {
        return await this.userModel.findOne({ rut: rut, password: password });
    }

    async createUser(createUserDto: CreateUserDTO): Promise<User> {
        const createdUser = new this.userModel(createUserDto);
        return createdUser.save();
    }

    async update(user: User): Promise<User> {
        return await this.userModel.updateOne({ User: user._id }, user);
    }

    async delete(id: string): Promise<User> {
        return await this.userModel.deleteOne({ _id: id });
    }
}