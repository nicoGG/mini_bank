import { User } from '../interface/user.interface';
import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';


export const UserSchema = new mongoose.Schema({
    rut: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    first_name: { type: String, required: true },
    last_name: { type: String, required: true },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
}, { versionKey: false });

// UserSchema.pre<User>('save', function save(next) {
//     let data = this;
//     if (!data.isModified('password')) return next();
//     bcrypt.genSalt(10, (err: any, salt: any) => {
//         if (err) return next(err);
//         bcrypt.hash(data.password, salt, (err: any, hash: any) => {
//             if (err) return next(err);
//             data.password = hash;
//             next();
//         });
//     });
// });

// UserSchema.methods.checkPassword = function (attempt, callback) {
//     let user = this;
//     bcrypt.compare(attempt, user.password, (err, isMatch) => {
//         if (err) return callback(err);
//         callback(null, isMatch);
//     });
// };