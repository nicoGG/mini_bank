import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { UserSchema } from './schemas/user.schema';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'User', schema: UserSchema, collection: 'users' }]),
        PassportModule.register({ defaultStrategy: 'jwt', session: false }),
    ],
    exports: [UserService],
    controllers: [UserController],
    providers: [UserService]
})
export class UserModule { }