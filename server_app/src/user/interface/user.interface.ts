import { Document } from 'mongoose';

export interface User extends Document {
    rut: string;
    password: string;
    first_name: string;
    last_name: string;
    created_at: Date;
    updated_at: Date;
}