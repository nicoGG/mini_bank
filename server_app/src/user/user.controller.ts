import { CreateUserDTO } from './dto/user.dto';
import { UserService } from './user.service';
import { Body, Controller, Delete, Get, HttpStatus, Inject, NotFoundException, Param, Patch, Post, Res } from '@nestjs/common';
import { ApiImplicitParam, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { User } from './interface/user.interface';

@Controller('users')
export class UserController {

    constructor(private service: UserService) { }

    @ApiUseTags('User')
    @Get('all')
    async GetAll() {
        return await this.service.findAll();
    }

    @ApiUseTags('User')
    @Get(':id')
    async GetById(@Param('id') id: string) {
        return this.service.find(id);
    }

    @ApiUseTags('User')
    @Post(':rut')
    async GetByRut(@Param('rut') rut: string) {
        return this.service.findOneByRut(rut);
    }

    @ApiUseTags('User')
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @Post('add')
    async Add(@Res() res, @Body() user: CreateUserDTO) {
        const req = await this.service.createUser(user);
        if (!req) {
            throw new NotFoundException('Customer does not exist!');
        }
        return res.status(HttpStatus.OK).json(req);
    }

    @ApiUseTags('User')
    @Patch('update')
    async Update(@Res() res, @Body() user: User) {
        const req = await this.service.update(user);
        return res.status(HttpStatus.OK).json(req);
    }

    @ApiUseTags('User')
    @ApiOperation({ title: 'Detele one User' })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Delete('delete/:id')
    async Delete(@Res() res, @Param('id') id: string) {
        const req = await this.service.delete(id);
        return res.status(HttpStatus.OK).json(req);
    }
}
