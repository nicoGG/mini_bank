import { ApiModelProperty } from "@nestjs/swagger";

export class UserLoginDto {

    @ApiModelProperty()
    readonly rut: string;

    @ApiModelProperty()
    readonly password: string;
}