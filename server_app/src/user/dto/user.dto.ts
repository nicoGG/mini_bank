import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class CreateUserDTO {
    @ApiModelProperty()
    readonly rut: string;

    @ApiModelProperty()
    readonly password: string;

    @ApiModelProperty()
    readonly first_name: string;

    @ApiModelProperty()
    readonly last_name: string;

    @ApiModelPropertyOptional({ type: Date, example: new Date() })
    created_at: Date;

    @ApiModelPropertyOptional({ type: Date, example: new Date() })
    updated_at: Date;
}