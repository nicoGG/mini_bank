import { UserModule } from './user/user.module';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { AuthModule } from './auth/auth.module';
import { TransactionModule } from './transaction/transaction.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://admin:admin@cluster0.63xto.mongodb.net/reign?retryWrites=true&w=majority'),
    PassportModule.register({ defaultStrategy: 'jwt', session: false }),
    AuthModule,
    UserModule,
    TransactionModule
  ],
})
export class AppModule { }