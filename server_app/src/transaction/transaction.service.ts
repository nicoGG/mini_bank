import { UserService } from 'src/user/user.service';
import { CreateTransactionDTO } from './dto/transaction.dto';
import { Transaction } from './interface/transaction.interface';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class TransactionService {

    constructor(
        @InjectModel('Transaction') private transactionModel: Model<Transaction>,
        private readonly userService: UserService
    ) { }

    async getAllTransactionsByRut(rut: string): Promise<Transaction[]> {
        return await this.transactionModel.find({ $or: [{ rut: rut }, { rut_receiver: rut }]}).sort({ created_at: -1 }).exec();
    }

    async addTransaction(createTransacrionDto: CreateTransactionDTO): Promise<Transaction> {
        const createTransaction = new this.transactionModel(createTransacrionDto);
        return createTransaction.save();
    }
}
