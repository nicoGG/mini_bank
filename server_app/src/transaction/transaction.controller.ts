import { CreateTransactionDTO } from './dto/transaction.dto';
import { TransactionService } from './transaction.service';
import { Body, Controller, HttpStatus, NotFoundException, Param, Res } from '@nestjs/common';
import { ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { Get, Post } from '@nestjs/common/decorators/http/request-mapping.decorator';

@Controller('transaction')
export class TransactionController {

    constructor(private service: TransactionService) { }

    @ApiUseTags('Transaction')
    @Get(':rut')
    async GetByRut(@Param('rut') rut: string) {
        return await this.service.getAllTransactionsByRut(rut);
    }

    @ApiUseTags('Transaction')
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @Post('add')
    async Add(@Res() res, @Body() transaction: CreateTransactionDTO) {
        const req = await this.service.addTransaction(transaction);
        if (!req) {
            throw new NotFoundException('User no exist!');
        }
        return res.status(HttpStatus.OK).json(req);
    }
}