import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger"
export class CreateTransactionDTO {

    @ApiModelProperty()
    rut: String;

    @ApiModelProperty()
    rut_receiver: String;

    @ApiModelProperty()
    first_name: String;
    
    @ApiModelProperty()
    last_name: String;

    @ApiModelProperty()
    description: String;

    @ApiModelProperty()
    type: Number;

    @ApiModelProperty()
    amount: Number;

    @ApiModelPropertyOptional({ type: Date, example: new Date() })
    created_at: Date;
}