import { ApiModelProperty } from "@nestjs/swagger";

export class UserValidateDTO {

    @ApiModelProperty()
    rut: String;

    @ApiModelProperty()
    first_name: String;

    @ApiModelProperty()
    last_name: String;
}