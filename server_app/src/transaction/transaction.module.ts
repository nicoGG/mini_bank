import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from 'src/user/user.module';
import { TransactionSchema } from './schemas/transaction.schema';
import { TransactionController } from './transaction.controller';
import { TransactionService } from './transaction.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Transaction', schema: TransactionSchema, collection: 'transactions' }]),
    UserModule,
  ],
  exports: [TransactionService],
  controllers: [TransactionController],
  providers: [TransactionService]
})
export class TransactionModule { }