import * as mongoose from 'mongoose';

export const TransactionSchema = new mongoose.Schema({
    id_user: String,
    rut: String,
    rut_receiver: String,
    first_name: String,
    last_name: String,
    description: String,
    type: Number,
    amount: Number,
    created_at: { type: Date, default: Date.now },
});
