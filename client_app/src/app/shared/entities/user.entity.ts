export class UserEntity {
    id?: number;
    rut?: string;
    first_name?: string;
    last_name?: string;
    password?: string;
    email?: string;
    createdAt?: Date;
    updatedAt?: Date;
    version?: number;
}