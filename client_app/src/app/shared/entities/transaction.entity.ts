export class TransactionEntity {
    _id: number;
    rut: string;
    rut_receiver: String;
    first_name: string;
    last_name: string;
    description: string;
    type: number;
    amount: number;
    created_at: Date;
}