export class TransferEntity {
    id: number;
    rut: string;
    number_account: number;
    first_name: string;
    last_name: string;
    amount: number
}