import { SidebarComponent } from './sidebar/sidebar.component';
import { AngularMaterialModule } from './../../angular-material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        AngularMaterialModule
    ],
    declarations: [
        HeaderComponent,
        SidebarComponent,
        FooterComponent,
    ],
    exports: [
        HeaderComponent,
        SidebarComponent,
        FooterComponent,
    ],
})
export class SharedComponentsModule { }