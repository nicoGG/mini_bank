import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TransactionDTO } from '../../../modules/transactions/dto/transaction.dto';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {

  constructor(
    private _httpClient: HttpClient
  ) { }

  makeTransaction(transactionDTO: TransactionDTO) {
    const endpoint: string = environment.api_url + 'transaction/add';
    return this._httpClient.post(endpoint, transactionDTO);
  }

  getUserTransactions(rut: string) {
    const endpoint: string = environment.api_url + 'transaction/' + rut;
    return this._httpClient.get(endpoint);
  }
}