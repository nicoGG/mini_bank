import { UserLoginDTO } from './../../../modules/login/dto/user-login.dto';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { UserDTO } from '../../../modules/login/dto/user.dto';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private _httpClient: HttpClient
  ) { }

  loginUser(userLogin: UserLoginDTO): Observable<any> {
    const endpoint: string = environment.api_url + 'auth/login';
    return this._httpClient.post(endpoint, userLogin);
  }

  registerUser(user: UserDTO): Observable<any> {
    const endpoint: string = environment.api_url + 'users/add';
    return this._httpClient.post(endpoint, user);
  }

  getUserByRut(rut: string) {
    const endpoint: string = environment.api_url + 'users/' + rut;
    return this._httpClient.post(endpoint, null);
  }
}