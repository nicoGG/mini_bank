import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserEntity } from '../../../../shared/entities/user.entity';
import chileanRut from 'chilean-rut';
import { AuthenticationService } from '../../../../core/http/authentication/authentication.service';
import { Subscription } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.component.html',
  styleUrls: ['./register-modal.component.css']
})
export class RegisterModalComponent implements OnInit {
  @ViewChild('rutInput') rutReference: ElementRef;
  form: FormGroup;
  hide = true;
  _Subscription: Subscription;

  constructor(
    @Inject(MAT_DIALOG_DATA) public _userEntity: UserEntity,
    public _dialogRef: MatDialogRef<RegisterModalComponent>,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private _authService: AuthenticationService
  ) {
    this._dialogRef.disableClose = true;
    this._userEntity = new UserEntity();
    this.createForm();
  }

  ngOnInit() { }

  createForm() {
    this.form = this.formBuilder.group({
      rut: ['', Validators.compose([Validators.required, Validators.maxLength(12)])],
      firstName: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(30)])],
      lastName: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(30)])],
      email: ['', Validators.compose([Validators.required, Validators.email, Validators.maxLength(300)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(30)])],
    });
  }

  onSubmit() {
    this._userEntity.rut = chileanRut.unformat(this.form.get('rut').value);
    this._userEntity.first_name = this.form.get('firstName').value;
    this._userEntity.last_name = this.form.get('lastName').value;
    this._userEntity.email = this.form.get('email').value;
    this._userEntity.password = this.form.get('password').value;
    this._dialogRef.close(this._userEntity);
  }

  close() {
    this._dialogRef.close();
  }

  formatRut() {
    if (this.form.get('rut').value) {
      if (this.form.get('rut').value.length === 9) {
        var dv = this.form.get('rut').value.substring(8);
        this.form.get('rut').setValue(chileanRut.format(this.form.get('rut').value, dv));
        this.validateRut(this.form.get('rut').value);
      } else if (this.form.get('rut').value.length === 8) {
        var dv = this.form.get('rut').value.substring(7);
        this.form.get('rut').setValue(chileanRut.format(this.form.get('rut').value, dv));
        this.validateRut(this.form.get('rut').value);
      } else {
        this._snackBar.open('El largo del rut no corresponde', null, { duration: 800 })
        this.form.get('rut').reset();
        this.rutReference.nativeElement.focus();
      }
    }
  }

  validateRut(rut: string) {
    if (!chileanRut.validate(rut)) {
      this._snackBar.open('Rut inválido', null, { duration: 800 })
      this.form.get('rut').reset();
      this.rutReference.nativeElement.focus();
    } else {
      this._Subscription = this._authService.getUserByRut(chileanRut.unformat(rut))
      .subscribe((user: UserEntity) => {
        if (user?.first_name) {
          this._snackBar.open('Rut ya registrado en el sistema', null, { duration: 3000 });
          this.form.get('rut').reset();
          this.rutReference.nativeElement.focus();
        }
      }, (httpErrorResponse: HttpErrorResponse) => {
        this.form.get('rut').reset();
        this.rutReference.nativeElement.focus();
      });
    }
  }

  getErrorMessageRut() {
    return this.form.get('rut').hasError('required') ? 'Debe ingresar un rut de usuario' :
      this.form.get('rut').hasError('minlength') ? 'El rut es muy corto' : '';
  }

  getErrorMessageFirstName() {
    return this.form.get('firstName').hasError('required') ? 'Debe ingresar un nombre' :
      this.form.get('firstName').hasError('minlength') ? 'El nombre es muy corto' : '';
  }

  getErrorMessageLastName() {
    return this.form.get('lastName').hasError('required') ? 'Debe ingresar un apellido' :
      this.form.get('lastName').hasError('minlength') ? 'El apellido es muy corto' : '';
  }

  getErrorMessageEmail() {
    return this.form.get('email').hasError('required') ? 'Debe ingresar un email' :
      this.form.get('email').hasError('email') ? 'Email inválido' : '';
  }

  getErrorMessagePassword() {
    return this.form.get('password').hasError('required') ? 'Debe ingresar una contraseña' :
      this.form.get('password').hasError('minlength') ? 'La contraseña es muy corta' : '';
  }
}