import { UserLoginDTO } from './../dto/user-login.dto';
import { Subscription } from 'rxjs';
import { animate, style, transition, trigger } from '@angular/animations';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../core/http/authentication/authentication.service';
import { MatDialog } from '@angular/material/dialog';
import { RegisterModalComponent } from '../modals/register-modal/register-modal.component';
import chileanRut from 'chilean-rut';
import { UserDTO } from '../dto/user.dto';
import { LocalStorageService } from '../../../core/services/local-storage/local-storage.service';
import { UserEntity } from 'src/app/shared/entities/user.entity';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [
    trigger('show', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate(500, style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate(500, style({ opacity: 0 }))
      ])
    ])
  ]
})
export class LoginComponent implements OnInit, OnDestroy {
  @ViewChild('rutInput') rutReference: ElementRef;
  _Subscription: Subscription;
  rut = new FormControl('', [Validators.required, Validators.maxLength(12)]);
  password = new FormControl('', [Validators.required, Validators.minLength(4)]);
  hide = true;
  loadingData = false;
  httpErrorResponse: HttpErrorResponse;
  urlLogo: string = 'assets/logo.png';

  constructor(
    private _router: Router,
    private _autenticationService: AuthenticationService,
    private _localStorageService: LocalStorageService,
    private _snackBar: MatSnackBar,
    private _dialog: MatDialog,
  ) { }

  ngOnInit() {
    if (this._localStorageService.get('data-user')) {
      this._localStorageService.clear();
    }
  }

  ngOnDestroy() {
    if (this._Subscription) {
      this._Subscription.unsubscribe();
    }
  }

  onSubmit() {
    if (this.rut.valid && this.password.valid) {
      this.loadingData = true;
      const userLogin: UserLoginDTO = {
        rut: chileanRut.unformat(this.rut.value),
        password: this.password.value
      };
      this._Subscription = this._autenticationService.loginUser(userLogin)
        .subscribe((response: any) => {
          this._localStorageService.set('data-user', JSON.stringify(response));
          this._router.navigate(['home-user']);
          this._snackBar.open('Bienvenido ' + response?.first_name + ' ' + response?.last_name, null, { duration: 3000 });
          this.loadingData = false;
        }, (httpErrorResponse: HttpErrorResponse) => {
          console.error(httpErrorResponse.message);
          switch (httpErrorResponse.status) {
            case 500:
              this._snackBar.open('ERROR ' + httpErrorResponse.status + ': ' + httpErrorResponse.message, null, { duration: 5000 });
              break;
            case 401:
              this._snackBar.open('Credenciales Incorrectas', null, { duration: 5000 });
              break;
            default:
              this._snackBar.open(httpErrorResponse.message, null, { duration: 5000 });
              break;
          }
          this.loadingData = false;
        });
    }
  }

  register() {
    const _dialogRegister = this._dialog.open(RegisterModalComponent, { width: '500px', data: null });
    _dialogRegister.afterClosed()
      .subscribe((user: UserEntity) => {
        let userDTO: UserDTO = {
          rut: user.rut,
          password: user.password,
          first_name: user.first_name,
          last_name: user.last_name,
          created_at: Date(),
          updated_at: Date()
        };
        if (user !== undefined) {
          this._Subscription = this._autenticationService.registerUser(userDTO)
            .subscribe((response: any) => {
              console.log('RESPONSE', response);
              this._snackBar.open(userDTO.first_name + ' ' + userDTO.last_name + ' registrado correctamente', null, { duration: 3000 });
            }, (httpErrorResponse: HttpErrorResponse) => {
              console.error(httpErrorResponse.message);
            });
        }
      });
  }

  formatRut() {
    if (this.rut.value) {
      if (this.rut.value.length === 9) {
        var dv = this.rut.value.substring(8);
        this.rut.setValue(chileanRut.format(this.rut.value, dv));
        this.validateRut(this.rut.value);
      } else if (this.rut.value.length === 8) {
        var dv = this.rut.value.substring(7);
        this.rut.setValue(chileanRut.format(this.rut.value, dv));
        this.validateRut(this.rut.value);
      } else {
        this._snackBar.open('El largo del rut no corresponde', null, { duration: 800 })
        this.rut.invalid;
        this.rut.reset();
        this.rutReference.nativeElement.focus();
      }
    }
  }

  validateRut(rut: string) {
    if (!chileanRut.validate(rut)) {
      this._snackBar.open('Rut inválido', null, { duration: 800 })
      this.rut.invalid;
      this.rut.reset();
      this.rutReference.nativeElement.focus();
    } else {
      this.rut.valid;
    }
  }

  getErrorMessageUser() {
    return this.rut.hasError('required') ? 'Debe ingresar un rut' : this.rut.hasError('minlength') ? 'El rut es muy corto' : '';
  }

  getErrorMessagePassword() {
    return this.password.hasError('required') ? 'Debe ingresar una contraseña' : this.password.hasError('minlength') ? 'La contraseña es muy corta' : '';
  }
}