export class UserDTO {
    rut: string;
    password: string;
    first_name: string;
    last_name: string;
    created_at: string;
    updated_at: string;
}