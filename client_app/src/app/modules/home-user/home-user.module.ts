import { TransferModalComponent } from './modals/transfer-modal/transfer-modal.component';
import { TransactionModalComponent } from './modals/transaction-modal/transaction-modal.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from './../../angular-material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeUserRoutingModule } from './home-user-routing.module';
import { HomeUserComponent } from './page/home-user.component';
import { NumberDirective } from 'src/app/shared/directives/numbers-only.directive';

@NgModule({
  declarations: [
    HomeUserComponent,
    TransactionModalComponent,
    TransferModalComponent,
    NumberDirective
  ],
  imports: [
    CommonModule,
    HomeUserRoutingModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  entryComponents:[
    TransactionModalComponent,
    TransferModalComponent
  ],
  exports:[
    HomeUserComponent
  ]
})
export class HomeUserModule { }