import { MatTableDataSource } from '@angular/material/table';
import { registerLocaleData } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { faMoneyBill } from '@fortawesome/free-solid-svg-icons';
import { MatDialog } from '@angular/material/dialog';
import es from '@angular/common/locales/es';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { TransactionModalComponent } from '../modals/transaction-modal/transaction-modal.component';
import { TransferModalComponent } from '../modals/transfer-modal/transfer-modal.component';
import { TransactionsService } from './../../../core/http/transactions/transactions.service';
import { LocalStorageService } from '../../../../app/core/services/local-storage/local-storage.service';
import { TransactionEntity } from '../../../shared/entities/transaction.entity';
import { TransactionDTO } from '../../transactions/dto/transaction.dto';
import { Subscription } from 'rxjs';
import { UserEntity } from '../../../shared/entities/user.entity';

@Component({
  selector: 'app-home-user',
  templateUrl: './home-user.component.html',
  styleUrls: ['./home-user.component.css']
})
export class HomeUserComponent implements OnInit, OnDestroy {
  famoneybill = faMoneyBill;
  _Subscription: Subscription;
  displayedColumns: string[] = ['id', 'description', 'receptor', 'type', 'date', 'amount',];
  dataSource = new MatTableDataSource([]);
  userData: UserEntity;
  loadingData: boolean = false;
  totalAmount: number = 0;

  constructor(
    private _localStoreService: LocalStorageService,
    private _transactionService: TransactionsService,
    private _snackBar: MatSnackBar,
    private _dialog: MatDialog,
  ) { }

  ngOnInit() {
    registerLocaleData(es);
    this.loadDataUser();
  }

  ngOnDestroy() {
    if (this._Subscription) {
      this._Subscription.unsubscribe();
    }
  }

  filter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  loadDataUser() {
    this.dataSource = new MatTableDataSource([]);
    this.userData = JSON.parse(this._localStoreService.get('data-user'));
    if (this.userData.rut) {
      this.loadingData = true;
      this._Subscription = this._transactionService.getUserTransactions(this.userData.rut)
        .subscribe((response: TransactionEntity[]) => {
          this.dataSource = new MatTableDataSource(response);
          let amounts: number = 0;
          response.forEach(element => {
            if (element.type == 0) {
              amounts += element.amount;
            }
            if (element.type == 1) {
              amounts -= element.amount;
            }
            if (element.rut == this.userData.rut && element.type == 2) {
              amounts -= element.amount;
            }
            if (element.rut_receiver == this.userData.rut && element.type == 2) {
              amounts += element.amount;
            }
          });
          this.totalAmount = amounts;
          this.loadingData = false;
        }, (httpErrorResponse: HttpErrorResponse) => {
          this.loadingData = false;
          console.error('ERROR', httpErrorResponse.message);
        });
    }
  }

  transaction(type: number) {
    let _transactionEntity = new TransactionEntity();
    _transactionEntity.type = type;
    _transactionEntity.rut = this.userData.rut;
    _transactionEntity.rut_receiver = this.userData.rut;
    _transactionEntity.amount = 0;
    const _dialogTransaction = this._dialog.open(TransactionModalComponent, { width: '500px', data: { transfer: _transactionEntity, total_amount: this.totalAmount } });
    _dialogTransaction.afterClosed()
      .subscribe((data: TransactionEntity) => {
        if (data !== undefined) {
          let transactionDTO = new TransactionDTO();
          transactionDTO.type = data.type;
          transactionDTO.rut = data.rut;
          transactionDTO.rut_receiver = this.userData.rut;
          transactionDTO.description = data.type == 0 ? 'Abono de saldo' : 'Retiro de saldo';
          transactionDTO.amount = data.amount;
          this._Subscription = this._transactionService.makeTransaction(transactionDTO)
            .subscribe((response: any) => {
              this._snackBar.open('Transacción realizada correctamente', null, { duration: 3000 });
              this.loadDataUser();
            }, (httpErrorResponse: HttpErrorResponse) => {
              console.error('ERROR', httpErrorResponse.message);
              this._snackBar.open('Error al realizar transaccion', null, { duration: 3000 });
            });
        }
      });
  }

  makeTransfer() {
    if (this.totalAmount > 0) {
      const _dialogTransfer = this._dialog.open(TransferModalComponent, { width: '500px', data: { total_amount: this.totalAmount } });
      _dialogTransfer.afterClosed()
        .subscribe((data: TransactionEntity) => {
          if (data !== undefined) {
            if (data?.rut !== this.userData?.rut) {
              let transactionDTO = new TransactionDTO();
              transactionDTO.rut = this.userData.rut;
              transactionDTO.rut_receiver = data?.rut;
              transactionDTO.first_name = data.first_name;
              transactionDTO.last_name = data.last_name;
              transactionDTO.description = data.description;
              transactionDTO.type = 2;
              transactionDTO.amount = data.amount;
              this._Subscription = this._transactionService.makeTransaction(transactionDTO)
                .subscribe((response: any) => {
                  this.loadDataUser();
                  this._snackBar.open('Transferencia realizada correctamente', null, { duration: 3000 });
                }, (httpErrorResponse: HttpErrorResponse) => {
                  console.error('ERROR', httpErrorResponse.message);
                  this._snackBar.open('Error al realizar transaccion', null, { duration: 3000 });
                });
            } else {
              this._snackBar.open('No te puedes transferir a ti mismo', null, { duration: 3000 });
            }
          }
        });
    } else {
      this._snackBar.open('No posees saldo suficiente para transferir', null, { duration: 3000 });
    }
  }
}