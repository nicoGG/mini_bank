import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TransactionEntity } from 'src/app/shared/entities/transaction.entity';

@Component({
  selector: 'app-transaction-modal',
  templateUrl: './transaction-modal.component.html',
  styleUrls: ['./transaction-modal.component.css']
})
export class TransactionModalComponent implements OnInit {
  title: string;
  form: FormGroup;
  _transactionEntity: TransactionEntity;
  total_amount: number;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public _dialogRef: MatDialogRef<TransactionModalComponent>,
    private formBuilder: FormBuilder,
  ) {
    this._dialogRef.disableClose = true;
    this._transactionEntity = new TransactionEntity();
    this.total_amount = data.total_amount;
    this._transactionEntity = data.transfer;
    this.createForm();
    if (this._transactionEntity.type == 0) {
      this.title = 'Cargar saldo';
    } else {
      this.title = 'Retitar monto';
    }
  }

  ngOnInit() { }

  createForm() {
    this.form = this.formBuilder.group({
      amount: ['', Validators.compose([Validators.required, Validators.min(1), Validators.max(this.total_amount), Validators.maxLength(12)])],
    });
    this.form.get('amount').valueChanges
      .subscribe(data => {
        if (this._transactionEntity.type == 0) {
          this.form.get('amount').setValidators([Validators.required, Validators.min(1)]);
        }
      });
  }

  onSubmit() {
    this._transactionEntity.rut = this._transactionEntity.rut;
    this._transactionEntity.type = this._transactionEntity.type;
    this._transactionEntity.amount = this.form.get('amount').value;
    this._dialogRef.close(this._transactionEntity);
  }

  getErrorMessageAmount() {
    return this.form.get('amount').hasError('required') ? 'Debe ingresar un monto' :
      this.form.get('amount').hasError('max') ? 'Exede el monto máximo ($' + this.total_amount + ')' : '';
  }

  close() {
    this._dialogRef.close();
  }
}