import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { TransactionEntity } from '../../../../shared/entities/transaction.entity';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, ElementRef, Inject, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthenticationService } from '../../../../core/http/authentication/authentication.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import chileanRut from 'chilean-rut';
import { UserEntity } from 'src/app/shared/entities/user.entity';

@Component({
  selector: 'app-transaction-modal',
  templateUrl: './transfer-modal.component.html',
  styleUrls: ['./transfer-modal.component.css']
})
export class TransferModalComponent implements OnInit, OnDestroy {
  @ViewChild('rutInput') rutReference: ElementRef;
  _transactionEntity: TransactionEntity;
  total_amount: number;
  form: FormGroup;
  _Subscription: Subscription;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public _dialogRef: MatDialogRef<TransferModalComponent>,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private _authService: AuthenticationService
  ) {
    this._dialogRef.disableClose = true;
    this._transactionEntity = new TransactionEntity();
    this.total_amount = data.total_amount;
    this.createForm();
  }

  ngOnInit() { }

  ngOnDestroy() {
    if (this._Subscription) {
      this._Subscription.unsubscribe();
    }
  }

  close() {
    this._dialogRef.close();
  }

  onSubmit() {
    this._transactionEntity.rut = chileanRut.unformat(this.form.get('rut').value);
    this._transactionEntity.description = this.form.get('description').value;
    this._transactionEntity.first_name = this.form.get('first_name').value;
    this._transactionEntity.last_name = this.form.get('last_name').value;
    this._transactionEntity.type = 1;
    this._transactionEntity.amount = this.form.get('amount').value;
    this._dialogRef.close(this._transactionEntity);
  }

  createForm() {
    this.form = this.formBuilder.group({
      rut: ['', Validators.compose([Validators.required, Validators.maxLength(12)])],
      description: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(30)])],
      first_name: [{ value: '', disabled: true }, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(30)])],
      last_name: [{ value: '', disabled: true }, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(30)])],
      amount: ['', Validators.compose([Validators.required, Validators.min(1), Validators.max(this.total_amount)])],
    });
  }

  cleanUserName() {
    this.form.get('first_name').reset();
    this.form.get('last_name').reset();
  }

  formatRut() {
    this.cleanUserName();
    if (this.form.get('rut').value) {
      if (this.form.get('rut').value.length === 9) {
        var dv = this.form.get('rut').value.substring(8);
        this.form.get('rut').setValue(chileanRut.format(this.form.get('rut').value, dv));
        this.validateRut(this.form.get('rut').value);
      } else if (this.form.get('rut').value.length === 8) {
        var dv = this.form.get('rut').value.substring(7);
        this.form.get('rut').setValue(chileanRut.format(this.form.get('rut').value, dv));
        this.validateRut(this.form.get('rut').value);
      } else {
        this._snackBar.open('El largo del rut no corresponde', null, { duration: 800 });
        this.form.get('rut').reset();
        this.rutReference.nativeElement.focus();
      }
    }
  }

  validateRut(rut: string) {
    if (!chileanRut.validate(rut)) {
      this._snackBar.open('Rut inválido', null, { duration: 800 })
      this.form.get('rut').reset();
      this.rutReference.nativeElement.focus();
    } else {
      this._Subscription = this._authService.getUserByRut(chileanRut.unformat(rut))
        .subscribe((user: UserEntity) => {
          if (user?.first_name) {
            this.form.get('first_name').setValue(user?.first_name);
            this.form.get('last_name').setValue(user?.last_name);
          } else {
            this._snackBar.open('Destinatario inexistente', null, { duration: 3000 });
            this.form.get('rut').reset();
            this.rutReference.nativeElement.focus();
            this.cleanUserName();
          }
        }, (httpErrorResponse: HttpErrorResponse) => {
          console.error(httpErrorResponse.message);
          this._snackBar.open('Destinatario inexistente', null, { duration: 3000 });
          this.form.get('rut').reset();
          this.rutReference.nativeElement.focus();
          this.cleanUserName();
        });
    }
  }

  getErrorMessageRut() {
    return this.form.get('rut').hasError('required') ? 'Debe ingresar un rut' :
      this.form.get('rut').hasError('minlength') ? 'El rut es muy corto' : '';
  }

  getErrorMessageDescription() {
    return this.form.get('description').hasError('required') ? 'Debe ingresar una descripción' :
      this.form.get('description').hasError('minlength') ? 'El la descripción es muy corta' : '';
  }

  getErrorMessageFirstName() {
    return this.form.get('first_name').hasError('required') ? 'Debe ingresar el nombre del destinatario' :
      this.form.get('first_name').hasError('minlength') ? 'El nombre es muy corto' : '';
  }

  getErrorMessageLastName() {
    return this.form.get('last_name').hasError('required') ? 'Debe ingresar el apellido del destinatario' :
      this.form.get('last_name').hasError('minlength') ? 'El apellido es muy corto' : '';
  }

  getErrorMessageAmount() {
    return this.form.get('amount').hasError('required') ? 'Debe ingresar un monto' :
      this.form.get('amount').hasError('max') ? 'Exede el monto máximo ($' + this.total_amount + ')' : '';
  }
}