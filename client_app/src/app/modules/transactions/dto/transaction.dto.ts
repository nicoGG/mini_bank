export class TransactionDTO {
    rut: String;
    rut_receiver: String;
    first_name: String;
    last_name: String;
    description: String;
    type: Number;
    amount: Number;
    created_at: Date;
}